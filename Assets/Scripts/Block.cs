﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (rigidbody.velocity.x != 0 || rigidbody.velocity.y != 0 || rigidbody.velocity.z != 0) {
			Destroy(gameObject, 1f);
		}
	}


	public static string GetUniqueID(){
		string key = "ID";
		var random = new System.Random();
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
		double timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
		string uniqueID = Application.systemLanguage //Language
				+"-"+timestamp //Time
				+"-"+Time.time*1000000 //Time in game
				+"-"+random.Next(1000000000); //random number
		Debug.Log("Generated Unique ID: "+uniqueID);
		if(PlayerPrefs.HasKey(key)){
			uniqueID = PlayerPrefs.GetString(key);
		} else {
			PlayerPrefs.SetString(key, uniqueID);
			PlayerPrefs.Save();
		}
		return uniqueID;
	}

	/*void OnCollisionEnter(Collision o) {
		Debug.Log (o.transform.tag);
	}*/
}
