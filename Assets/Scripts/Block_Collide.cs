﻿using UnityEngine;
using System.Collections;

public class Block_Collide : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider o) {
		if (o.rigidbody && o.tag == "renchon") {
			o.rigidbody.AddForce((transform.position - o.transform.position)*-40f, ForceMode.Acceleration);
			Destroy(o.gameObject, 1f);
		}
	}
}
