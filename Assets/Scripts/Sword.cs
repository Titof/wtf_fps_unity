﻿using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour {

	Animator a;

	// Use this for initialization
	void Start () {
		a = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			a.SetBool ("attack", true);
		} else {
			a.SetBool("attack", false);
		}
	}
}
