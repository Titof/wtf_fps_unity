﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	float Integrity;
	public float percentDestroy = 30f;
	public float actualPercent;

	// Use this for initialization
	void Start () {
		Integrity = transform.childCount;
		actualPercent = (transform.childCount * 100) / Integrity;
	}
	
	// Update is called once per frame
	void Update () {
		actualPercent = (transform.childCount * 100) / Integrity;
		if (actualPercent <= percentDestroy) {
			Destroy(transform.parent.gameObject);
		}
	}
}
